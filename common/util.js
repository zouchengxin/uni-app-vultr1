var baseUrl="http://39.106.207.193:8000";
module.exports={
	get:function(url,data,successFun){
		let params={
			url:baseUrl+url,
			method:"GET",
			data:data,
			dataType:"json",
			header:{
				
			},
			fail:function(){
				console.error("[GET]请求"+url+"接口时出错!");
				successFun([]);
			},
			success:function(data,statusCode,header){
				if(statusCode!=200&&statusCode!=undefined){
					console.log("[GET]url"+"接口返回数据不正确");
				}else{
					successFun(data);
				}
			}
		};
		uni.request(params);
	},
	post:function(url,data,successFun){
		let params={
			url:baseUrl+url,
			method:"POST",
			data:data,
			dataType:"json",
			fail:function(){
				console.error("[POST]请求"+url+"接口时出错!");
			},
			success:function(data,statusCode,header){
				if(statusCode!=200&&statusCode!=undefined){
					console.log("[POST]url"+"接口返回数据不正确");
				}else{
					successFun(data);
				}
			}
		};
		uni.request(params);
	},
};