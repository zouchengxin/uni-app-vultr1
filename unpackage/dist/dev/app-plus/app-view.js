var __pageFrameStartTime__ = Date.now();
var __webviewId__;
var __wxAppCode__ = {};
var __WXML_GLOBAL__ = {
  entrys: {},
  defines: {},
  modules: {},
  ops: [],
  wxs_nf_init: undefined,
  total_ops: 0
};
var $gwx;

/*v0.5vv_20190312_syb_scopedata*/window.__wcc_version__='v0.5vv_20190312_syb_scopedata';window.__wcc_version_info__={"customComponents":true,"fixZeroRpx":true,"propValueDeepCopy":false};
var $gwxc
var $gaic={}
$gwx=function(path,global){
if(typeof global === 'undefined') global={};if(typeof __WXML_GLOBAL__ === 'undefined') {__WXML_GLOBAL__={};
}__WXML_GLOBAL__.modules = __WXML_GLOBAL__.modules || {};
function _(a,b){if(typeof(b)!='undefined')a.children.push(b);}
function _v(k){if(typeof(k)!='undefined')return {tag:'virtual','wxKey':k,children:[]};return {tag:'virtual',children:[]};}
function _n(tag){$gwxc++;if($gwxc>=16000){throw 'Dom limit exceeded, please check if there\'s any mistake you\'ve made.'};return {tag:'wx-'+tag,attr:{},children:[],n:[],raw:{},generics:{}}}
function _p(a,b){b&&a.properities.push(b);}
function _s(scope,env,key){return typeof(scope[key])!='undefined'?scope[key]:env[key]}
function _wp(m){console.warn("WXMLRT_$gwx:"+m)}
function _wl(tname,prefix){_wp(prefix+':-1:-1:-1: Template `' + tname + '` is being called recursively, will be stop.')}
$gwn=console.warn;
$gwl=console.log;
function $gwh()
{
function x()
{
}
x.prototype = 
{
hn: function( obj, all )
{
if( typeof(obj) == 'object' )
{
var cnt=0;
var any1=false,any2=false;
for(var x in obj)
{
any1=any1|x==='__value__';
any2=any2|x==='__wxspec__';
cnt++;
if(cnt>2)break;
}
return cnt == 2 && any1 && any2 && ( all || obj.__wxspec__ !== 'm' || this.hn(obj.__value__) === 'h' ) ? "h" : "n";
}
return "n";
},
nh: function( obj, special )
{
return { __value__: obj, __wxspec__: special ? special : true }
},
rv: function( obj )
{
return this.hn(obj,true)==='n'?obj:this.rv(obj.__value__);
},
hm: function( obj )
{
if( typeof(obj) == 'object' )
{
var cnt=0;
var any1=false,any2=false;
for(var x in obj)
{
any1=any1|x==='__value__';
any2=any2|x==='__wxspec__';
cnt++;
if(cnt>2)break;
}
return cnt == 2 && any1 && any2 && (obj.__wxspec__ === 'm' || this.hm(obj.__value__) );
}
return false;
}
}
return new x;
}
wh=$gwh();
function $gstack(s){
var tmp=s.split('\n '+' '+' '+' ');
for(var i=0;i<tmp.length;++i){
if(0==i) continue;
if(")"===tmp[i][tmp[i].length-1])
tmp[i]=tmp[i].replace(/\s\(.*\)$/,"");
else
tmp[i]="at anonymous function";
}
return tmp.join('\n '+' '+' '+' ');
}
function $gwrt( should_pass_type_info )
{
function ArithmeticEv( ops, e, s, g, o )
{
var _f = false;
var rop = ops[0][1];
var _a,_b,_c,_d, _aa, _bb;
switch( rop )
{
case '?:':
_a = rev( ops[1], e, s, g, o, _f );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? rev( ops[2], e, s, g, o, _f ) : rev( ops[3], e, s, g, o, _f );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '&&':
_a = rev( ops[1], e, s, g, o, _f );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? rev( ops[2], e, s, g, o, _f ) : wh.rv( _a );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '||':
_a = rev( ops[1], e, s, g, o, _f );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? wh.rv(_a) : rev( ops[2], e, s, g, o, _f );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '+':
case '*':
case '/':
case '%':
case '|':
case '^':
case '&':
case '===':
case '==':
case '!=':
case '!==':
case '>=':
case '<=':
case '>':
case '<':
case '<<':
case '>>':
_a = rev( ops[1], e, s, g, o, _f );
_b = rev( ops[2], e, s, g, o, _f );
_c = should_pass_type_info && (wh.hn( _a ) === 'h' || wh.hn( _b ) === 'h');
switch( rop )
{
case '+':
_d = wh.rv( _a ) + wh.rv( _b );
break;
case '*':
_d = wh.rv( _a ) * wh.rv( _b );
break;
case '/':
_d = wh.rv( _a ) / wh.rv( _b );
break;
case '%':
_d = wh.rv( _a ) % wh.rv( _b );
break;
case '|':
_d = wh.rv( _a ) | wh.rv( _b );
break;
case '^':
_d = wh.rv( _a ) ^ wh.rv( _b );
break;
case '&':
_d = wh.rv( _a ) & wh.rv( _b );
break;
case '===':
_d = wh.rv( _a ) === wh.rv( _b );
break;
case '==':
_d = wh.rv( _a ) == wh.rv( _b );
break;
case '!=':
_d = wh.rv( _a ) != wh.rv( _b );
break;
case '!==':
_d = wh.rv( _a ) !== wh.rv( _b );
break;
case '>=':
_d = wh.rv( _a ) >= wh.rv( _b );
break;
case '<=':
_d = wh.rv( _a ) <= wh.rv( _b );
break;
case '>':
_d = wh.rv( _a ) > wh.rv( _b );
break;
case '<':
_d = wh.rv( _a ) < wh.rv( _b );
break;
case '<<':
_d = wh.rv( _a ) << wh.rv( _b );
break;
case '>>':
_d = wh.rv( _a ) >> wh.rv( _b );
break;
default:
break;
}
return _c ? wh.nh( _d, "c" ) : _d;
break;
case '-':
_a = ops.length === 3 ? rev( ops[1], e, s, g, o, _f ) : 0;
_b = ops.length === 3 ? rev( ops[2], e, s, g, o, _f ) : rev( ops[1], e, s, g, o, _f );
_c = should_pass_type_info && (wh.hn( _a ) === 'h' || wh.hn( _b ) === 'h');
_d = _c ? wh.rv( _a ) - wh.rv( _b ) : _a - _b;
return _c ? wh.nh( _d, "c" ) : _d;
break;
case '!':
_a = rev( ops[1], e, s, g, o, _f );
_c = should_pass_type_info && (wh.hn( _a ) == 'h');
_d = !wh.rv(_a);
return _c ? wh.nh( _d, "c" ) : _d;
case '~':
_a = rev( ops[1], e, s, g, o, _f );
_c = should_pass_type_info && (wh.hn( _a ) == 'h');
_d = ~wh.rv(_a);
return _c ? wh.nh( _d, "c" ) : _d;
default:
$gwn('unrecognized op' + rop );
}
}
function rev( ops, e, s, g, o, newap )
{
var op = ops[0];
var _f = false;
if ( typeof newap !== "undefined" ) o.ap = newap;
if( typeof(op)==='object' )
{
var vop=op[0];
var _a, _aa, _b, _bb, _c, _d, _s, _e, _ta, _tb, _td;
switch(vop)
{
case 2:
return ArithmeticEv(ops,e,s,g,o);
break;
case 4: 
return rev( ops[1], e, s, g, o, _f );
break;
case 5: 
switch( ops.length )
{
case 2: 
_a = rev( ops[1],e,s,g,o,_f );
return should_pass_type_info?[_a]:[wh.rv(_a)];
return [_a];
break;
case 1: 
return [];
break;
default:
_a = rev( ops[1],e,s,g,o,_f );
_b = rev( ops[2],e,s,g,o,_f );
_a.push( 
should_pass_type_info ?
_b :
wh.rv( _b )
);
return _a;
break;
}
break;
case 6:
_a = rev(ops[1],e,s,g,o);
var ap = o.ap;
_ta = wh.hn(_a)==='h';
_aa = _ta ? wh.rv(_a) : _a;
o.is_affected |= _ta;
if( should_pass_type_info )
{
if( _aa===null || typeof(_aa) === 'undefined' )
{
return _ta ? wh.nh(undefined, 'e') : undefined;
}
_b = rev(ops[2],e,s,g,o,_f);
_tb = wh.hn(_b) === 'h';
_bb = _tb ? wh.rv(_b) : _b;
o.ap = ap;
o.is_affected |= _tb;
if( _bb===null || typeof(_bb) === 'undefined' || 
_bb === "__proto__" || _bb === "prototype" || _bb === "caller" ) 
{
return (_ta || _tb) ? wh.nh(undefined, 'e') : undefined;
}
_d = _aa[_bb];
if ( typeof _d === 'function' && !ap ) _d = undefined;
_td = wh.hn(_d)==='h';
o.is_affected |= _td;
return (_ta || _tb) ? (_td ? _d : wh.nh(_d, 'e')) : _d;
}
else
{
if( _aa===null || typeof(_aa) === 'undefined' )
{
return undefined;
}
_b = rev(ops[2],e,s,g,o,_f);
_tb = wh.hn(_b) === 'h';
_bb = _tb ? wh.rv(_b) : _b;
o.ap = ap;
o.is_affected |= _tb;
if( _bb===null || typeof(_bb) === 'undefined' || 
_bb === "__proto__" || _bb === "prototype" || _bb === "caller" ) 
{
return undefined;
}
_d = _aa[_bb];
if ( typeof _d === 'function' && !ap ) _d = undefined;
_td = wh.hn(_d)==='h';
o.is_affected |= _td;
return _td ? wh.rv(_d) : _d;
}
case 7: 
switch(ops[1][0])
{
case 11:
o.is_affected |= wh.hn(g)==='h';
return g;
case 3:
_s = wh.rv( s );
_e = wh.rv( e );
_b = ops[1][1];
if (g && g.f && g.f.hasOwnProperty(_b) )
{
_a = g.f;
o.ap = true;
}
else
{
_a = _s && _s.hasOwnProperty(_b) ? 
s : (_e && _e.hasOwnProperty(_b) ? e : undefined );
}
if( should_pass_type_info )
{
if( _a )
{
_ta = wh.hn(_a) === 'h';
_aa = _ta ? wh.rv( _a ) : _a;
_d = _aa[_b];
_td = wh.hn(_d) === 'h';
o.is_affected |= _ta || _td;
_d = _ta && !_td ? wh.nh(_d,'e') : _d;
return _d;
}
}
else
{
if( _a )
{
_ta = wh.hn(_a) === 'h';
_aa = _ta ? wh.rv( _a ) : _a;
_d = _aa[_b];
_td = wh.hn(_d) === 'h';
o.is_affected |= _ta || _td;
return wh.rv(_d);
}
}
return undefined;
}
break;
case 8: 
_a = {};
_a[ops[1]] = rev(ops[2],e,s,g,o,_f);
return _a;
break;
case 9: 
_a = rev(ops[1],e,s,g,o,_f);
_b = rev(ops[2],e,s,g,o,_f);
function merge( _a, _b, _ow )
{
var ka, _bbk;
_ta = wh.hn(_a)==='h';
_tb = wh.hn(_b)==='h';
_aa = wh.rv(_a);
_bb = wh.rv(_b);
for(var k in _bb)
{
if ( _ow || !_aa.hasOwnProperty(k) )
{
_aa[k] = should_pass_type_info ? (_tb ? wh.nh(_bb[k],'e') : _bb[k]) : wh.rv(_bb[k]);
}
}
return _a;
}
var _c = _a
var _ow = true
if ( typeof(ops[1][0]) === "object" && ops[1][0][0] === 10 ) {
_a = _b
_b = _c
_ow = false
}
if ( typeof(ops[1][0]) === "object" && ops[1][0][0] === 10 ) {
var _r = {}
return merge( merge( _r, _a, _ow ), _b, _ow );
}
else
return merge( _a, _b, _ow );
break;
case 10:
_a = rev(ops[1],e,s,g,o,_f);
_a = should_pass_type_info ? _a : wh.rv( _a );
return _a ;
break;
case 12:
var _r;
_a = rev(ops[1],e,s,g,o);
if ( !o.ap )
{
return should_pass_type_info && wh.hn(_a)==='h' ? wh.nh( _r, 'f' ) : _r;
}
var ap = o.ap;
_b = rev(ops[2],e,s,g,o,_f);
o.ap = ap;
_ta = wh.hn(_a)==='h';
_tb = _ca(_b);
_aa = wh.rv(_a);	
_bb = wh.rv(_b); snap_bb=$gdc(_bb,"nv_");
try{
_r = typeof _aa === "function" ? $gdc(_aa.apply(null, snap_bb)) : undefined;
} catch (e){
e.message = e.message.replace(/nv_/g,"");
e.stack = e.stack.substring(0,e.stack.indexOf("\n", e.stack.lastIndexOf("at nv_")));
e.stack = e.stack.replace(/\snv_/g," "); 
e.stack = $gstack(e.stack);	
if(g.debugInfo)
{
e.stack += "\n "+" "+" "+" at "+g.debugInfo[0]+":"+g.debugInfo[1]+":"+g.debugInfo[2];
console.error(e);
}
_r = undefined;
}
return should_pass_type_info && (_tb || _ta) ? wh.nh( _r, 'f' ) : _r;
}
}
else
{
if( op === 3 || op === 1) return ops[1];
else if( op === 11 ) 
{
var _a='';
for( var i = 1 ; i < ops.length ; i++ )
{
var xp = wh.rv(rev(ops[i],e,s,g,o,_f));
_a += typeof(xp) === 'undefined' ? '' : xp;
}
return _a;
}
}
}
function wrapper( ops, e, s, g, o, newap )
{
if( ops[0] == '11182016' )
{
g.debugInfo = ops[2];
return rev( ops[1], e, s, g, o, newap );
}
else
{
g.debugInfo = null;
return rev( ops, e, s, g, o, newap );
}
}
return wrapper;
}
gra=$gwrt(true); 
grb=$gwrt(false); 
function TestTest( expr, ops, e,s,g, expect_a, expect_b, expect_affected )
{
{
var o = {is_affected:false};
var a = gra( ops, e,s,g, o );
if( JSON.stringify(a) != JSON.stringify( expect_a )
|| o.is_affected != expect_affected )
{
console.warn( "A. " + expr + " get result " + JSON.stringify(a) + ", " + o.is_affected + ", but " + JSON.stringify( expect_a ) + ", " + expect_affected + " is expected" );
}
}
{
var o = {is_affected:false};
var a = grb( ops, e,s,g, o );
if( JSON.stringify(a) != JSON.stringify( expect_b )
|| o.is_affected != expect_affected )
{
console.warn( "B. " + expr + " get result " + JSON.stringify(a) + ", " + o.is_affected + ", but " + JSON.stringify( expect_b ) + ", " + expect_affected + " is expected" );
}
}
}

function wfor( to_iter, func, env, _s, global, father, itemname, indexname, keyname )
{
var _n = wh.hn( to_iter ) === 'n'; 
var scope = wh.rv( _s ); 
var has_old_item = scope.hasOwnProperty(itemname);
var has_old_index = scope.hasOwnProperty(indexname);
var old_item = scope[itemname];
var old_index = scope[indexname];
var full = Object.prototype.toString.call(wh.rv(to_iter));
var type = full[8]; 
if( type === 'N' && full[10] === 'l' ) type = 'X'; 
var _y;
if( _n )
{
if( type === 'A' ) 
{
var r_iter_item;
for( var i = 0 ; i < to_iter.length ; i++ )
{
scope[itemname] = to_iter[i];
scope[indexname] = _n ? i : wh.nh(i, 'h');
r_iter_item = wh.rv(to_iter[i]);
var key = keyname && r_iter_item ? (keyname==="*this" ? r_iter_item : wh.rv(r_iter_item[keyname])) : undefined;
_y = _v(key);
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'O' ) 
{
var i = 0;
var r_iter_item;
for( var k in to_iter )
{
scope[itemname] = to_iter[k];
scope[indexname] = _n ? k : wh.nh(k, 'h');
r_iter_item = wh.rv(to_iter[k]);
var key = keyname && r_iter_item ? (keyname==="*this" ? r_iter_item : wh.rv(r_iter_item[keyname])) : undefined;
_y = _v(key);
_(father,_y);
func( env,scope,_y,global );
i++;
}
}
else if( type === 'S' ) 
{
for( var i = 0 ; i < to_iter.length ; i++ )
{
scope[itemname] = to_iter[i];
scope[indexname] = _n ? i : wh.nh(i, 'h');
_y = _v( to_iter[i] + i );
_(father,_y);
func( env,scope,_y,global );
}
}
else if( type === 'N' ) 
{
for( var i = 0 ; i < to_iter ; i++ )
{
scope[itemname] = i;
scope[indexname] = _n ? i : wh.nh(i, 'h');
_y = _v( i );
_(father,_y);
func(env,scope,_y,global);
}
}
else
{
}
}
else
{
var r_to_iter = wh.rv(to_iter);
var r_iter_item, iter_item;
if( type === 'A' ) 
{
for( var i = 0 ; i < r_to_iter.length ; i++ )
{
iter_item = r_to_iter[i];
iter_item = wh.hn(iter_item)==='n' ? wh.nh(iter_item,'h') : iter_item;
r_iter_item = wh.rv( iter_item );
scope[itemname] = iter_item
scope[indexname] = _n ? i : wh.nh(i, 'h');
var key = keyname && r_iter_item ? (keyname==="*this" ? r_iter_item : wh.rv(r_iter_item[keyname])) : undefined;
_y = _v(key);
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'O' ) 
{
var i=0;
for( var k in r_to_iter )
{
iter_item = r_to_iter[k];
iter_item = wh.hn(iter_item)==='n'? wh.nh(iter_item,'h') : iter_item;
r_iter_item = wh.rv( iter_item );
scope[itemname] = iter_item;
scope[indexname] = _n ? k : wh.nh(k, 'h');
var key = keyname && r_iter_item ? (keyname==="*this" ? r_iter_item : wh.rv(r_iter_item[keyname])) : undefined;
_y=_v(key);
_(father,_y);
func( env, scope, _y, global );
i++
}
}
else if( type === 'S' ) 
{
for( var i = 0 ; i < r_to_iter.length ; i++ )
{
iter_item = wh.nh(r_to_iter[i],'h');
scope[itemname] = iter_item;
scope[indexname] = _n ? i : wh.nh(i, 'h');
_y = _v( to_iter[i] + i );
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'N' ) 
{
for( var i = 0 ; i < r_to_iter ; i++ )
{
iter_item = wh.nh(i,'h');
scope[itemname] = iter_item;
scope[indexname]= _n ? i : wh.nh(i,'h');
_y = _v( i );
_(father,_y);
func(env,scope,_y,global);
}
}
else
{
}
}
if(has_old_item)
{
scope[itemname]=old_item;
}
else
{
delete scope[itemname];
}
if(has_old_index)
{
scope[indexname]=old_index;
}
else
{
delete scope[indexname];
}
}

function _ca(o)
{ 
if ( wh.hn(o) == 'h' ) return true;
if ( typeof o !== "object" ) return false;
for(var i in o){ 
if ( o.hasOwnProperty(i) ){
if (_ca(o[i])) return true;
}
}
return false;
}
function _da( node, attrname, opindex, raw, o )
{
var isaffected = false;
var value = $gdc( raw, "", 2 );
if ( o.ap && value && value.constructor===Function ) 
{
attrname = "$wxs:" + attrname; 
node.attr["$gdc"] = $gdc;
}
if ( o.is_affected || _ca(raw) ) 
{
node.n.push( attrname );
node.raw[attrname] = raw;
}
node.attr[attrname] = value;
}
function _r( node, attrname, opindex, env, scope, global ) 
{
global.opindex=opindex;
var o = {}, _env;
var a = grb( z[opindex], env, scope, global, o );
_da( node, attrname, opindex, a, o );
}
function _rz( z, node, attrname, opindex, env, scope, global ) 
{
global.opindex=opindex;
var o = {}, _env;
var a = grb( z[opindex], env, scope, global, o );
_da( node, attrname, opindex, a, o );
}
function _o( opindex, env, scope, global )
{
global.opindex=opindex;
var nothing = {};
var r = grb( z[opindex], env, scope, global, nothing );
return (r&&r.constructor===Function) ? undefined : r;
}
function _oz( z, opindex, env, scope, global )
{
global.opindex=opindex;
var nothing = {};
var r = grb( z[opindex], env, scope, global, nothing );
return (r&&r.constructor===Function) ? undefined : r;
}
function _1( opindex, env, scope, global, o )
{
var o = o || {};
global.opindex=opindex;
return gra( z[opindex], env, scope, global, o );
}
function _1z( z, opindex, env, scope, global, o )
{
var o = o || {};
global.opindex=opindex;
return gra( z[opindex], env, scope, global, o );
}
function _2( opindex, func, env, scope, global, father, itemname, indexname, keyname )
{
var o = {};
var to_iter = _1( opindex, env, scope, global );
wfor( to_iter, func, env, scope, global, father, itemname, indexname, keyname );
}
function _2z( z, opindex, func, env, scope, global, father, itemname, indexname, keyname )
{
var o = {};
var to_iter = _1z( z, opindex, env, scope, global );
wfor( to_iter, func, env, scope, global, father, itemname, indexname, keyname );
}


function _m(tag,attrs,generics,env,scope,global)
{
var tmp=_n(tag);
var base=0;
for(var i = 0 ; i < attrs.length ; i+=2 )
{
if(base+attrs[i+1]<0)
{
tmp.attr[attrs[i]]=true;
}
else
{
_r(tmp,attrs[i],base+attrs[i+1],env,scope,global);
if(base===0)base=attrs[i+1];
}
}
for(var i=0;i<generics.length;i+=2)
{
if(base+generics[i+1]<0)
{
tmp.generics[generics[i]]="";
}
else
{
var $t=grb(z[base+generics[i+1]],env,scope,global);
if ($t!="") $t="wx-"+$t;
tmp.generics[generics[i]]=$t;
if(base===0)base=generics[i+1];
}
}
return tmp;
}
function _mz(z,tag,attrs,generics,env,scope,global)
{
var tmp=_n(tag);
var base=0;
for(var i = 0 ; i < attrs.length ; i+=2 )
{
if(base+attrs[i+1]<0)
{
tmp.attr[attrs[i]]=true;
}
else
{
_rz(z, tmp,attrs[i],base+attrs[i+1],env,scope,global);
if(base===0)base=attrs[i+1];
}
}
for(var i=0;i<generics.length;i+=2)
{
if(base+generics[i+1]<0)
{
tmp.generics[generics[i]]="";
}
else
{
var $t=grb(z[base+generics[i+1]],env,scope,global);
if ($t!="") $t="wx-"+$t;
tmp.generics[generics[i]]=$t;
if(base===0)base=generics[i+1];
}
}
return tmp;
}

var nf_init=function(){
if(typeof __WXML_GLOBAL__==="undefined"||undefined===__WXML_GLOBAL__.wxs_nf_init){
nf_init_Object();nf_init_Function();nf_init_Array();nf_init_String();nf_init_Boolean();nf_init_Number();nf_init_Math();nf_init_Date();nf_init_RegExp();
}
if(typeof __WXML_GLOBAL__!=="undefined") __WXML_GLOBAL__.wxs_nf_init=true;
};
var nf_init_Object=function(){
Object.defineProperty(Object.prototype,"nv_constructor",{writable:true,value:"Object"})
Object.defineProperty(Object.prototype,"nv_toString",{writable:true,value:function(){return "[object Object]"}})
}
var nf_init_Function=function(){
Object.defineProperty(Function.prototype,"nv_constructor",{writable:true,value:"Function"})
Object.defineProperty(Function.prototype,"nv_length",{get:function(){return this.length;},set:function(){}});
Object.defineProperty(Function.prototype,"nv_toString",{writable:true,value:function(){return "[function Function]"}})
}
var nf_init_Array=function(){
Object.defineProperty(Array.prototype,"nv_toString",{writable:true,value:function(){return this.nv_join();}})
Object.defineProperty(Array.prototype,"nv_join",{writable:true,value:function(s){
s=undefined==s?',':s;
var r="";
for(var i=0;i<this.length;++i){
if(0!=i) r+=s;
if(null==this[i]||undefined==this[i]) r+='';	
else if(typeof this[i]=='function') r+=this[i].nv_toString();
else if(typeof this[i]=='object'&&this[i].nv_constructor==="Array") r+=this[i].nv_join();
else r+=this[i].toString();
}
return r;
}})
Object.defineProperty(Array.prototype,"nv_constructor",{writable:true,value:"Array"})
Object.defineProperty(Array.prototype,"nv_concat",{writable:true,value:Array.prototype.concat})
Object.defineProperty(Array.prototype,"nv_pop",{writable:true,value:Array.prototype.pop})
Object.defineProperty(Array.prototype,"nv_push",{writable:true,value:Array.prototype.push})
Object.defineProperty(Array.prototype,"nv_reverse",{writable:true,value:Array.prototype.reverse})
Object.defineProperty(Array.prototype,"nv_shift",{writable:true,value:Array.prototype.shift})
Object.defineProperty(Array.prototype,"nv_slice",{writable:true,value:Array.prototype.slice})
Object.defineProperty(Array.prototype,"nv_sort",{writable:true,value:Array.prototype.sort})
Object.defineProperty(Array.prototype,"nv_splice",{writable:true,value:Array.prototype.splice})
Object.defineProperty(Array.prototype,"nv_unshift",{writable:true,value:Array.prototype.unshift})
Object.defineProperty(Array.prototype,"nv_indexOf",{writable:true,value:Array.prototype.indexOf})
Object.defineProperty(Array.prototype,"nv_lastIndexOf",{writable:true,value:Array.prototype.lastIndexOf})
Object.defineProperty(Array.prototype,"nv_every",{writable:true,value:Array.prototype.every})
Object.defineProperty(Array.prototype,"nv_some",{writable:true,value:Array.prototype.some})
Object.defineProperty(Array.prototype,"nv_forEach",{writable:true,value:Array.prototype.forEach})
Object.defineProperty(Array.prototype,"nv_map",{writable:true,value:Array.prototype.map})
Object.defineProperty(Array.prototype,"nv_filter",{writable:true,value:Array.prototype.filter})
Object.defineProperty(Array.prototype,"nv_reduce",{writable:true,value:Array.prototype.reduce})
Object.defineProperty(Array.prototype,"nv_reduceRight",{writable:true,value:Array.prototype.reduceRight})
Object.defineProperty(Array.prototype,"nv_length",{get:function(){return this.length;},set:function(value){this.length=value;}});
}
var nf_init_String=function(){
Object.defineProperty(String.prototype,"nv_constructor",{writable:true,value:"String"})
Object.defineProperty(String.prototype,"nv_toString",{writable:true,value:String.prototype.toString})
Object.defineProperty(String.prototype,"nv_valueOf",{writable:true,value:String.prototype.valueOf})
Object.defineProperty(String.prototype,"nv_charAt",{writable:true,value:String.prototype.charAt})
Object.defineProperty(String.prototype,"nv_charCodeAt",{writable:true,value:String.prototype.charCodeAt})
Object.defineProperty(String.prototype,"nv_concat",{writable:true,value:String.prototype.concat})
Object.defineProperty(String.prototype,"nv_indexOf",{writable:true,value:String.prototype.indexOf})
Object.defineProperty(String.prototype,"nv_lastIndexOf",{writable:true,value:String.prototype.lastIndexOf})
Object.defineProperty(String.prototype,"nv_localeCompare",{writable:true,value:String.prototype.localeCompare})
Object.defineProperty(String.prototype,"nv_match",{writable:true,value:String.prototype.match})
Object.defineProperty(String.prototype,"nv_replace",{writable:true,value:String.prototype.replace})
Object.defineProperty(String.prototype,"nv_search",{writable:true,value:String.prototype.search})
Object.defineProperty(String.prototype,"nv_slice",{writable:true,value:String.prototype.slice})
Object.defineProperty(String.prototype,"nv_split",{writable:true,value:String.prototype.split})
Object.defineProperty(String.prototype,"nv_substring",{writable:true,value:String.prototype.substring})
Object.defineProperty(String.prototype,"nv_toLowerCase",{writable:true,value:String.prototype.toLowerCase})
Object.defineProperty(String.prototype,"nv_toLocaleLowerCase",{writable:true,value:String.prototype.toLocaleLowerCase})
Object.defineProperty(String.prototype,"nv_toUpperCase",{writable:true,value:String.prototype.toUpperCase})
Object.defineProperty(String.prototype,"nv_toLocaleUpperCase",{writable:true,value:String.prototype.toLocaleUpperCase})
Object.defineProperty(String.prototype,"nv_trim",{writable:true,value:String.prototype.trim})
Object.defineProperty(String.prototype,"nv_length",{get:function(){return this.length;},set:function(value){this.length=value;}});
}
var nf_init_Boolean=function(){
Object.defineProperty(Boolean.prototype,"nv_constructor",{writable:true,value:"Boolean"})
Object.defineProperty(Boolean.prototype,"nv_toString",{writable:true,value:Boolean.prototype.toString})
Object.defineProperty(Boolean.prototype,"nv_valueOf",{writable:true,value:Boolean.prototype.valueOf})
}
var nf_init_Number=function(){
Object.defineProperty(Number,"nv_MAX_VALUE",{writable:false,value:Number.MAX_VALUE})
Object.defineProperty(Number,"nv_MIN_VALUE",{writable:false,value:Number.MIN_VALUE})
Object.defineProperty(Number,"nv_NEGATIVE_INFINITY",{writable:false,value:Number.NEGATIVE_INFINITY})
Object.defineProperty(Number,"nv_POSITIVE_INFINITY",{writable:false,value:Number.POSITIVE_INFINITY})
Object.defineProperty(Number.prototype,"nv_constructor",{writable:true,value:"Number"})
Object.defineProperty(Number.prototype,"nv_toString",{writable:true,value:Number.prototype.toString})
Object.defineProperty(Number.prototype,"nv_toLocaleString",{writable:true,value:Number.prototype.toLocaleString})
Object.defineProperty(Number.prototype,"nv_valueOf",{writable:true,value:Number.prototype.valueOf})
Object.defineProperty(Number.prototype,"nv_toFixed",{writable:true,value:Number.prototype.toFixed})
Object.defineProperty(Number.prototype,"nv_toExponential",{writable:true,value:Number.prototype.toExponential})
Object.defineProperty(Number.prototype,"nv_toPrecision",{writable:true,value:Number.prototype.toPrecision})
}
var nf_init_Math=function(){
Object.defineProperty(Math,"nv_E",{writable:false,value:Math.E})
Object.defineProperty(Math,"nv_LN10",{writable:false,value:Math.LN10})
Object.defineProperty(Math,"nv_LN2",{writable:false,value:Math.LN2})
Object.defineProperty(Math,"nv_LOG2E",{writable:false,value:Math.LOG2E})
Object.defineProperty(Math,"nv_LOG10E",{writable:false,value:Math.LOG10E})
Object.defineProperty(Math,"nv_PI",{writable:false,value:Math.PI})
Object.defineProperty(Math,"nv_SQRT1_2",{writable:false,value:Math.SQRT1_2})
Object.defineProperty(Math,"nv_SQRT2",{writable:false,value:Math.SQRT2})
Object.defineProperty(Math,"nv_abs",{writable:false,value:Math.abs})
Object.defineProperty(Math,"nv_acos",{writable:false,value:Math.acos})
Object.defineProperty(Math,"nv_asin",{writable:false,value:Math.asin})
Object.defineProperty(Math,"nv_atan",{writable:false,value:Math.atan})
Object.defineProperty(Math,"nv_atan2",{writable:false,value:Math.atan2})
Object.defineProperty(Math,"nv_ceil",{writable:false,value:Math.ceil})
Object.defineProperty(Math,"nv_cos",{writable:false,value:Math.cos})
Object.defineProperty(Math,"nv_exp",{writable:false,value:Math.exp})
Object.defineProperty(Math,"nv_floor",{writable:false,value:Math.floor})
Object.defineProperty(Math,"nv_log",{writable:false,value:Math.log})
Object.defineProperty(Math,"nv_max",{writable:false,value:Math.max})
Object.defineProperty(Math,"nv_min",{writable:false,value:Math.min})
Object.defineProperty(Math,"nv_pow",{writable:false,value:Math.pow})
Object.defineProperty(Math,"nv_random",{writable:false,value:Math.random})
Object.defineProperty(Math,"nv_round",{writable:false,value:Math.round})
Object.defineProperty(Math,"nv_sin",{writable:false,value:Math.sin})
Object.defineProperty(Math,"nv_sqrt",{writable:false,value:Math.sqrt})
Object.defineProperty(Math,"nv_tan",{writable:false,value:Math.tan})
}
var nf_init_Date=function(){
Object.defineProperty(Date.prototype,"nv_constructor",{writable:true,value:"Date"})
Object.defineProperty(Date,"nv_parse",{writable:true,value:Date.parse})
Object.defineProperty(Date,"nv_UTC",{writable:true,value:Date.UTC})
Object.defineProperty(Date,"nv_now",{writable:true,value:Date.now})
Object.defineProperty(Date.prototype,"nv_toString",{writable:true,value:Date.prototype.toString})
Object.defineProperty(Date.prototype,"nv_toDateString",{writable:true,value:Date.prototype.toDateString})
Object.defineProperty(Date.prototype,"nv_toTimeString",{writable:true,value:Date.prototype.toTimeString})
Object.defineProperty(Date.prototype,"nv_toLocaleString",{writable:true,value:Date.prototype.toLocaleString})
Object.defineProperty(Date.prototype,"nv_toLocaleDateString",{writable:true,value:Date.prototype.toLocaleDateString})
Object.defineProperty(Date.prototype,"nv_toLocaleTimeString",{writable:true,value:Date.prototype.toLocaleTimeString})
Object.defineProperty(Date.prototype,"nv_valueOf",{writable:true,value:Date.prototype.valueOf})
Object.defineProperty(Date.prototype,"nv_getTime",{writable:true,value:Date.prototype.getTime})
Object.defineProperty(Date.prototype,"nv_getFullYear",{writable:true,value:Date.prototype.getFullYear})
Object.defineProperty(Date.prototype,"nv_getUTCFullYear",{writable:true,value:Date.prototype.getUTCFullYear})
Object.defineProperty(Date.prototype,"nv_getMonth",{writable:true,value:Date.prototype.getMonth})
Object.defineProperty(Date.prototype,"nv_getUTCMonth",{writable:true,value:Date.prototype.getUTCMonth})
Object.defineProperty(Date.prototype,"nv_getDate",{writable:true,value:Date.prototype.getDate})
Object.defineProperty(Date.prototype,"nv_getUTCDate",{writable:true,value:Date.prototype.getUTCDate})
Object.defineProperty(Date.prototype,"nv_getDay",{writable:true,value:Date.prototype.getDay})
Object.defineProperty(Date.prototype,"nv_getUTCDay",{writable:true,value:Date.prototype.getUTCDay})
Object.defineProperty(Date.prototype,"nv_getHours",{writable:true,value:Date.prototype.getHours})
Object.defineProperty(Date.prototype,"nv_getUTCHours",{writable:true,value:Date.prototype.getUTCHours})
Object.defineProperty(Date.prototype,"nv_getMinutes",{writable:true,value:Date.prototype.getMinutes})
Object.defineProperty(Date.prototype,"nv_getUTCMinutes",{writable:true,value:Date.prototype.getUTCMinutes})
Object.defineProperty(Date.prototype,"nv_getSeconds",{writable:true,value:Date.prototype.getSeconds})
Object.defineProperty(Date.prototype,"nv_getUTCSeconds",{writable:true,value:Date.prototype.getUTCSeconds})
Object.defineProperty(Date.prototype,"nv_getMilliseconds",{writable:true,value:Date.prototype.getMilliseconds})
Object.defineProperty(Date.prototype,"nv_getUTCMilliseconds",{writable:true,value:Date.prototype.getUTCMilliseconds})
Object.defineProperty(Date.prototype,"nv_getTimezoneOffset",{writable:true,value:Date.prototype.getTimezoneOffset})
Object.defineProperty(Date.prototype,"nv_setTime",{writable:true,value:Date.prototype.setTime})
Object.defineProperty(Date.prototype,"nv_setMilliseconds",{writable:true,value:Date.prototype.setMilliseconds})
Object.defineProperty(Date.prototype,"nv_setUTCMilliseconds",{writable:true,value:Date.prototype.setUTCMilliseconds})
Object.defineProperty(Date.prototype,"nv_setSeconds",{writable:true,value:Date.prototype.setSeconds})
Object.defineProperty(Date.prototype,"nv_setUTCSeconds",{writable:true,value:Date.prototype.setUTCSeconds})
Object.defineProperty(Date.prototype,"nv_setMinutes",{writable:true,value:Date.prototype.setMinutes})
Object.defineProperty(Date.prototype,"nv_setUTCMinutes",{writable:true,value:Date.prototype.setUTCMinutes})
Object.defineProperty(Date.prototype,"nv_setHours",{writable:true,value:Date.prototype.setHours})
Object.defineProperty(Date.prototype,"nv_setUTCHours",{writable:true,value:Date.prototype.setUTCHours})
Object.defineProperty(Date.prototype,"nv_setDate",{writable:true,value:Date.prototype.setDate})
Object.defineProperty(Date.prototype,"nv_setUTCDate",{writable:true,value:Date.prototype.setUTCDate})
Object.defineProperty(Date.prototype,"nv_setMonth",{writable:true,value:Date.prototype.setMonth})
Object.defineProperty(Date.prototype,"nv_setUTCMonth",{writable:true,value:Date.prototype.setUTCMonth})
Object.defineProperty(Date.prototype,"nv_setFullYear",{writable:true,value:Date.prototype.setFullYear})
Object.defineProperty(Date.prototype,"nv_setUTCFullYear",{writable:true,value:Date.prototype.setUTCFullYear})
Object.defineProperty(Date.prototype,"nv_toUTCString",{writable:true,value:Date.prototype.toUTCString})
Object.defineProperty(Date.prototype,"nv_toISOString",{writable:true,value:Date.prototype.toISOString})
Object.defineProperty(Date.prototype,"nv_toJSON",{writable:true,value:Date.prototype.toJSON})
}
var nf_init_RegExp=function(){
Object.defineProperty(RegExp.prototype,"nv_constructor",{writable:true,value:"RegExp"})
Object.defineProperty(RegExp.prototype,"nv_exec",{writable:true,value:RegExp.prototype.exec})
Object.defineProperty(RegExp.prototype,"nv_test",{writable:true,value:RegExp.prototype.test})
Object.defineProperty(RegExp.prototype,"nv_toString",{writable:true,value:RegExp.prototype.toString})
Object.defineProperty(RegExp.prototype,"nv_source",{get:function(){return this.source;},set:function(){}});
Object.defineProperty(RegExp.prototype,"nv_global",{get:function(){return this.global;},set:function(){}});
Object.defineProperty(RegExp.prototype,"nv_ignoreCase",{get:function(){return this.ignoreCase;},set:function(){}});
Object.defineProperty(RegExp.prototype,"nv_multiline",{get:function(){return this.multiline;},set:function(){}});
Object.defineProperty(RegExp.prototype,"nv_lastIndex",{get:function(){return this.lastIndex;},set:function(v){this.lastIndex=v;}});
}
nf_init();
var nv_getDate=function(){var args=Array.prototype.slice.call(arguments);args.unshift(Date);return new(Function.prototype.bind.apply(Date, args));}
var nv_getRegExp=function(){var args=Array.prototype.slice.call(arguments);args.unshift(RegExp);return new(Function.prototype.bind.apply(RegExp, args));}
var nv_console={}
nv_console.nv_log=function(){var res="WXSRT:";for(var i=0;i<arguments.length;++i)res+=arguments[i]+" ";console.log(res);}
var nv_parseInt = parseInt, nv_parseFloat = parseFloat, nv_isNaN = isNaN, nv_isFinite = isFinite, nv_decodeURI = decodeURI, nv_decodeURIComponent = decodeURIComponent, nv_encodeURI = encodeURI, nv_encodeURIComponent = encodeURIComponent;
function $gdc(o,p,r) {
o=wh.rv(o);
if(o===null||o===undefined) return o;
if(o.constructor===String||o.constructor===Boolean||o.constructor===Number) return o;
if(o.constructor===Object){
var copy={};
for(var k in o)
if(o.hasOwnProperty(k))
if(undefined===p) copy[k.substring(3)]=$gdc(o[k],p,r);
else copy[p+k]=$gdc(o[k],p,r);
return copy;
}
if(o.constructor===Array){
var copy=[];
for(var i=0;i<o.length;i++) copy.push($gdc(o[i],p,r));
return copy;
}
if(o.constructor===Date){
var copy=new Date();
copy.setTime(o.getTime());
return copy;
}
if(o.constructor===RegExp){
var f="";
if(o.global) f+="g";
if(o.ignoreCase) f+="i";
if(o.multiline) f+="m";
return (new RegExp(o.source,f));
}
if(r&&o.constructor===Function){
if ( r == 1 ) return $gdc(o(),undefined, 2);
if ( r == 2 ) return o;
}
return null;
}
var nv_JSON={}
nv_JSON.nv_stringify=function(o){
JSON.stringify(o);
return JSON.stringify($gdc(o));
}
nv_JSON.nv_parse=function(o){
if(o===undefined) return undefined;
var t=JSON.parse(o);
return $gdc(t,'nv_');
}

function _af(p, a, c){
p.extraAttr = {"t_action": a, "t_cid": c};
}

function _gv( )
{if( typeof( window.__webview_engine_version__) == 'undefined' ) return 0.0;
return window.__webview_engine_version__;}
function _ai(i,p,e,me,r,c){var x=_grp(p,e,me);if(x)i.push(x);else{i.push('');_wp(me+':import:'+r+':'+c+': Path `'+p+'` not found from `'+me+'`.')}}
function _grp(p,e,me){if(p[0]!='/'){var mepart=me.split('/');mepart.pop();var ppart=p.split('/');for(var i=0;i<ppart.length;i++){if( ppart[i]=='..')mepart.pop();else if(!ppart[i]||ppart[i]=='.')continue;else mepart.push(ppart[i]);}p=mepart.join('/');}if(me[0]=='.'&&p[0]=='/')p='.'+p;if(e[p])return p;if(e[p+'.wxml'])return p+'.wxml';}
function _gd(p,c,e,d){if(!c)return;if(d[p][c])return d[p][c];for(var x=e[p].i.length-1;x>=0;x--){if(e[p].i[x]&&d[e[p].i[x]][c])return d[e[p].i[x]][c]};for(var x=e[p].ti.length-1;x>=0;x--){var q=_grp(e[p].ti[x],e,p);if(q&&d[q][c])return d[q][c]}var ii=_gapi(e,p);for(var x=0;x<ii.length;x++){if(ii[x]&&d[ii[x]][c])return d[ii[x]][c]}for(var k=e[p].j.length-1;k>=0;k--)if(e[p].j[k]){for(var q=e[e[p].j[k]].ti.length-1;q>=0;q--){var pp=_grp(e[e[p].j[k]].ti[q],e,p);if(pp&&d[pp][c]){return d[pp][c]}}}}
function _gapi(e,p){if(!p)return [];if($gaic[p]){return $gaic[p]};var ret=[],q=[],h=0,t=0,put={},visited={};q.push(p);visited[p]=true;t++;while(h<t){var a=q[h++];for(var i=0;i<e[a].ic.length;i++){var nd=e[a].ic[i];var np=_grp(nd,e,a);if(np&&!visited[np]){visited[np]=true;q.push(np);t++;}}for(var i=0;a!=p&&i<e[a].ti.length;i++){var ni=e[a].ti[i];var nm=_grp(ni,e,a);if(nm&&!put[nm]){put[nm]=true;ret.push(nm);}}}$gaic[p]=ret;return ret;}
var $ixc={};function _ic(p,ent,me,e,s,r,gg){var x=_grp(p,ent,me);ent[me].j.push(x);if(x){if($ixc[x]){_wp('-1:include:-1:-1: `'+p+'` is being included in a loop, will be stop.');return;}$ixc[x]=true;try{ent[x].f(e,s,r,gg)}catch(e){}$ixc[x]=false;}else{_wp(me+':include:-1:-1: Included path `'+p+'` not found from `'+me+'`.')}}
function _w(tn,f,line,c){_wp(f+':template:'+line+':'+c+': Template `'+tn+'` not found.');}function _ev(dom){var changed=false;delete dom.properities;delete dom.n;if(dom.children){do{changed=false;var newch = [];for(var i=0;i<dom.children.length;i++){var ch=dom.children[i];if( ch.tag=='virtual'){changed=true;for(var j=0;ch.children&&j<ch.children.length;j++){newch.push(ch.children[j]);}}else { newch.push(ch); } } dom.children = newch; }while(changed);for(var i=0;i<dom.children.length;i++){_ev(dom.children[i]);}} return dom; }
function _tsd( root )
{
if( root.tag == "wx-wx-scope" ) 
{
root.tag = "virtual";
root.wxCkey = "11";
root['wxScopeData'] = root.attr['wx:scope-data'];
delete root.n;
delete root.raw;
delete root.generics;
delete root.attr;
}
for( var i = 0 ; root.children && i < root.children.length ; i++ )
{
_tsd( root.children[i] );
}
return root;
}

var e_={}
if(typeof(global.entrys)==='undefined')global.entrys={};e_=global.entrys;
var d_={}
if(typeof(global.defines)==='undefined')global.defines={};d_=global.defines;
var f_={}
if(typeof(global.modules)==='undefined')global.modules={};f_=global.modules || {};
var p_={}
__WXML_GLOBAL__.ops_cached = __WXML_GLOBAL__.ops_cached || {}
__WXML_GLOBAL__.ops_set = __WXML_GLOBAL__.ops_set || {};
__WXML_GLOBAL__.ops_init = __WXML_GLOBAL__.ops_init || {};
var z=__WXML_GLOBAL__.ops_set.$gwx || [];
function gz$gwx_1(){
if( __WXML_GLOBAL__.ops_cached.$gwx_1)return __WXML_GLOBAL__.ops_cached.$gwx_1
__WXML_GLOBAL__.ops_cached.$gwx_1=[];
(function(z){var a=11;function Z(ops){z.push(ops)}
Z([3,'search data-v-6e03cdbc'])
Z([[2,'+'],[[2,'+'],[1,'background-color:'],[[7],[3,'backgroundColor']]],[1,';']])
Z([3,'content data-v-6e03cdbc'])
Z([[2,'+'],[[2,'+'],[[2,'+'],[1,'border-radius:'],[[2,'+'],[[7],[3,'radius']],[1,'px']]],[1,';']],[[2,'+'],[[2,'+'],[1,'border:'],[[7],[3,'border']]],[1,';']]])
Z([[4],[[5],[[5],[1,'content-box data-v-6e03cdbc']],[[2,'?:'],[[2,'==='],[[7],[3,'mode']],[1,2]],[1,'center'],[1,'']]]])
Z([3,'icon icon-search data-v-6e03cdbc'])
Z([3,''])
Z([3,'__e'])
Z(z[7])
Z(z[7])
Z([[4],[[5],[[5],[1,'input data-v-6e03cdbc']],[[2,'?:'],[[2,'&&'],[[2,'!'],[[7],[3,'active']]],[[2,'==='],[[7],[3,'mode']],[1,2]]],[1,'center'],[1,'']]]])
Z([[4],[[5],[[5],[[5],[[4],[[5],[[5],[1,'focus']],[[4],[[5],[[4],[[5],[[5],[1,'focus']],[[4],[[5],[1,'$event']]]]]]]]]],[[4],[[5],[[5],[1,'blur']],[[4],[[5],[[4],[[5],[[5],[1,'blur']],[[4],[[5],[1,'$event']]]]]]]]]],[[4],[[5],[[5],[1,'input']],[[4],[[5],[[4],[[5],[[5],[1,'__set_model']],[[4],[[5],[[5],[[5],[[5],[1,'']],[1,'inputVal']],[1,'$event']],[[4],[[5]]]]]]]]]]]]])
Z([[7],[3,'isFocus']])
Z([[7],[3,'placeholder']])
Z([[7],[3,'inputVal']])
Z([[7],[3,'isDelShow']])
Z(z[7])
Z([3,'icon icon-del data-v-6e03cdbc'])
Z([[4],[[5],[[4],[[5],[[5],[1,'tap']],[[4],[[5],[[4],[[5],[[5],[1,'clear']],[[4],[[5],[1,'$event']]]]]]]]]]])
Z([3,''])
Z(z[7])
Z([3,'searchBtn data-v-6e03cdbc'])
Z([[4],[[5],[[4],[[5],[[5],[1,'tap']],[[4],[[5],[[4],[[5],[[5],[1,'search']],[[4],[[5],[1,'$event']]]]]]]]]]])
Z([[2,'!'],[[2,'||'],[[2,'&&'],[[2,'&&'],[[7],[3,'active']],[[7],[3,'show']]],[[2,'==='],[[7],[3,'button']],[1,'inside']]],[[2,'&&'],[[7],[3,'isDelShow']],[[2,'==='],[[7],[3,'button']],[1,'inside']]]]])
Z([3,'搜索'])
Z([[2,'==='],[[7],[3,'button']],[1,'outside']])
Z(z[7])
Z([[4],[[5],[[5],[1,'button data-v-6e03cdbc']],[[2,'?:'],[[2,'||'],[[7],[3,'show']],[[7],[3,'active']]],[1,'active'],[1,'']]]])
Z(z[22])
Z([3,'button-item data-v-6e03cdbc'])
Z([a,[[2,'?:'],[[2,'!'],[[7],[3,'show']]],[[7],[3,'searchName']],[1,'搜索']]])
})(__WXML_GLOBAL__.ops_cached.$gwx_1);return __WXML_GLOBAL__.ops_cached.$gwx_1
}
function gz$gwx_2(){
if( __WXML_GLOBAL__.ops_cached.$gwx_2)return __WXML_GLOBAL__.ops_cached.$gwx_2
__WXML_GLOBAL__.ops_cached.$gwx_2=[];
(function(z){var a=11;function Z(ops){z.push(ops)}
Z([3,'header'])
Z([[2,'!='],[[7],[3,'type']],[1,0]])
Z([3,'title'])
Z([a,[[2,'+'],[1,'最新'],[[6],[[7],[3,'types']],[[7],[3,'type']]]]])
Z(z[2])
Z([a,[[6],[[7],[3,'types']],[[7],[3,'type']]]])
Z([3,'__e'])
Z([3,'more'])
Z([[4],[[5],[[4],[[5],[[5],[1,'tap']],[[4],[[5],[[4],[[5],[[5],[1,'more']],[[4],[[5],[1,'$event']]]]]]]]]]])
Z([3,'更多\x3e'])
Z([3,'_hr'])
Z([3,'true/'])
Z([3,'4rpx'])
Z([3,'flex-row'])
Z([3,'index'])
Z([3,'item'])
Z([[7],[3,'sliceContents']])
Z(z[14])
Z(z[6])
Z([3,'flex-row-item'])
Z([[4],[[5],[[4],[[5],[[5],[1,'tap']],[[4],[[5],[[4],[[5],[[5],[1,'clickImage']],[[4],[[5],[[7],[3,'index']]]]]]]]]]]])
Z([3,'flex-row-item_image'])
Z([3,'aspectFill'])
Z([[6],[[7],[3,'item']],[3,'thumb']])
Z([3,'flex-row-item_title'])
Z([a,[[6],[[7],[3,'item']],[3,'title']]])
Z(z[10])
Z(z[1])
Z(z[6])
Z(z[7])
Z(z[8])
Z([a,[[2,'+'],[[2,'+'],[1,'查看更多'],[[6],[[7],[3,'types']],[[7],[3,'type']]]],[1,' \x3e']]])
})(__WXML_GLOBAL__.ops_cached.$gwx_2);return __WXML_GLOBAL__.ops_cached.$gwx_2
}
function gz$gwx_3(){
if( __WXML_GLOBAL__.ops_cached.$gwx_3)return __WXML_GLOBAL__.ops_cached.$gwx_3
__WXML_GLOBAL__.ops_cached.$gwx_3=[];
(function(z){var a=11;function Z(ops){z.push(ops)}
Z([3,'true'])
Z(z[0])
Z([3,'index'])
Z([3,'item'])
Z([[7],[3,'images']])
Z(z[2])
Z([3,'__e'])
Z([[4],[[5],[[4],[[5],[[5],[1,'tap']],[[4],[[5],[[4],[[5],[[5],[[5],[1,'clickImage']],[[4],[[5],[1,'$0']]]],[[4],[[5],[[4],[[5],[[4],[[5],[[5],[[5],[[5],[1,'images']],[1,'']],[[7],[3,'index']]],[1,'id']]]]]]]]]]]]]]])
Z([3,'aspectFill'])
Z([[6],[[7],[3,'item']],[3,'src']])
Z([3,'width:100%;height:150px;'])
})(__WXML_GLOBAL__.ops_cached.$gwx_3);return __WXML_GLOBAL__.ops_cached.$gwx_3
}
function gz$gwx_4(){
if( __WXML_GLOBAL__.ops_cached.$gwx_4)return __WXML_GLOBAL__.ops_cached.$gwx_4
__WXML_GLOBAL__.ops_cached.$gwx_4=[];
(function(z){var a=11;function Z(ops){z.push(ops)}
Z([3,'header'])
Z([3,'header-image'])
Z([3,'aspectFill'])
Z([[6],[[7],[3,'movie']],[3,'thumb']])
Z([3,'header-texts'])
Z([3,'one'])
Z([a,[[6],[[7],[3,'movie']],[3,'title']]])
Z([3,'two'])
Z([3,'gray-text'])
Z([3,'主演:'])
Z([a,[[6],[[7],[3,'movie']],[3,'actors']]])
Z(z[7])
Z(z[8])
Z([3,'导演:'])
Z([a,[[6],[[7],[3,'movie']],[3,'director']]])
Z(z[7])
Z(z[8])
Z([3,'地区:'])
Z([a,[[6],[[7],[3,'movie']],[3,'area']]])
Z(z[7])
Z(z[8])
Z([3,'类型:'])
Z([a,[[6],[[7],[3,'types']],[[2,'-'],[[6],[[7],[3,'movie']],[3,'type']],[1,1]]]])
Z(z[7])
Z(z[8])
Z([3,'年份:'])
Z([a,[[6],[[7],[3,'movie']],[3,'year']]])
Z([3,'intro-box'])
Z([3,'top'])
Z([3,'剧情介绍:'])
Z([3,'_hr'])
Z([3,'intro'])
Z([[2,'+'],[[2,'+'],[1,'height:'],[[2,'+'],[[7],[3,'introHeight']],[1,'rpx']]],[1,';']])
Z(z[8])
Z([3,'简介:'])
Z([a,[[6],[[7],[3,'movie']],[3,'introduction']]])
Z([[7],[3,'isMore']])
Z([3,'__e'])
Z([3,'more'])
Z([[4],[[5],[[4],[[5],[[5],[1,'tap']],[[4],[[5],[[4],[[5],[[5],[1,'more']],[[4],[[5],[1,'$event']]]]]]]]]]])
Z([[2,'!'],[[2,'>'],[[7],[3,'introHeight']],[1,304]]])
Z([3,'点击展开'])
Z(z[37])
Z(z[38])
Z(z[39])
Z(z[40])
Z([3,'点击收拢'])
Z([3,'btn-group'])
Z(z[37])
Z([[4],[[5],[[4],[[5],[[5],[1,'tap']],[[4],[[5],[[4],[[5],[[5],[1,'play']],[[4],[[5],[1,'$event']]]]]]]]]]])
Z([3,'点击观看'])
Z(z[37])
Z([[4],[[5],[[4],[[5],[[5],[1,'tap']],[[4],[[5],[[4],[[5],[[5],[1,'collect']],[[4],[[5],[1,'$event']]]]]]]]]]])
Z([3,'点击收藏'])
})(__WXML_GLOBAL__.ops_cached.$gwx_4);return __WXML_GLOBAL__.ops_cached.$gwx_4
}
function gz$gwx_5(){
if( __WXML_GLOBAL__.ops_cached.$gwx_5)return __WXML_GLOBAL__.ops_cached.$gwx_5
__WXML_GLOBAL__.ops_cached.$gwx_5=[];
(function(z){var a=11;function Z(ops){z.push(ops)}
Z([3,'webView'])
Z([[2,'!'],[[7],[3,'webviewSrc']]])
Z([[7],[3,'webviewSrc']])
Z([[7],[3,'webviewStyles']])
})(__WXML_GLOBAL__.ops_cached.$gwx_5);return __WXML_GLOBAL__.ops_cached.$gwx_5
}
function gz$gwx_6(){
if( __WXML_GLOBAL__.ops_cached.$gwx_6)return __WXML_GLOBAL__.ops_cached.$gwx_6
__WXML_GLOBAL__.ops_cached.$gwx_6=[];
(function(z){var a=11;function Z(ops){z.push(ops)}
Z([3,'__e'])
Z([3,'video-js vue-ref'])
Z([[4],[[5],[[4],[[5],[[5],[1,'error']],[[4],[[5],[[4],[[5],[[5],[1,'playError']],[[4],[[5],[1,'$event']]]]]]]]]]])
Z([3,'video'])
Z(z[3])
Z([3,'fill'])
Z([[7],[3,'playSrc']])
Z([3,'name'])
Z([3,'value'])
Z([[7],[3,'sourceArr']])
Z(z[7])
Z([3,'source'])
Z(z[7])
Z([a,[[7],[3,'name']]])
Z([3,'_hr'])
Z([3,'true/'])
Z([3,'4rpx'])
Z([3,'flex-box'])
Z([3,'index'])
Z([3,'item'])
Z([[7],[3,'value']])
Z(z[18])
Z(z[0])
Z([3,'flex-item'])
Z([[4],[[5],[[4],[[5],[[5],[1,'tap']],[[4],[[5],[[4],[[5],[[5],[[5],[1,'clickPart']],[[4],[[5],[1,'$0']]]],[[4],[[5],[[4],[[5],[[5],[[4],[[5],[[5],[[5],[1,'sourceArr']],[1,'']],[[7],[3,'name']]]]],[[4],[[5],[[5],[[5],[[5],[1,'']],[1,'']],[[7],[3,'index']]],[1,'url']]]]]]]]]]]]]]])
Z([a,[[6],[[7],[3,'item']],[3,'part']]])
})(__WXML_GLOBAL__.ops_cached.$gwx_6);return __WXML_GLOBAL__.ops_cached.$gwx_6
}
function gz$gwx_7(){
if( __WXML_GLOBAL__.ops_cached.$gwx_7)return __WXML_GLOBAL__.ops_cached.$gwx_7
__WXML_GLOBAL__.ops_cached.$gwx_7=[];
(function(z){var a=11;function Z(ops){z.push(ops)}
})(__WXML_GLOBAL__.ops_cached.$gwx_7);return __WXML_GLOBAL__.ops_cached.$gwx_7
}
function gz$gwx_8(){
if( __WXML_GLOBAL__.ops_cached.$gwx_8)return __WXML_GLOBAL__.ops_cached.$gwx_8
__WXML_GLOBAL__.ops_cached.$gwx_8=[];
(function(z){var a=11;function Z(ops){z.push(ops)}
})(__WXML_GLOBAL__.ops_cached.$gwx_8);return __WXML_GLOBAL__.ops_cached.$gwx_8
}
function gz$gwx_9(){
if( __WXML_GLOBAL__.ops_cached.$gwx_9)return __WXML_GLOBAL__.ops_cached.$gwx_9
__WXML_GLOBAL__.ops_cached.$gwx_9=[];
(function(z){var a=11;function Z(ops){z.push(ops)}
Z([3,'__l'])
Z([3,'__e'])
Z([[4],[[5],[[4],[[5],[[5],[1,'^search']],[[4],[[5],[[4],[[5],[[5],[1,'search']],[[4],[[5],[1,'$event']]]]]]]]]]])
Z([3,'1'])
Z(z[0])
Z(z[1])
Z([[4],[[5],[[4],[[5],[[5],[1,'^clickImage']],[[4],[[5],[[4],[[5],[1,'clickImage']]]]]]]]])
Z([[7],[3,'swiperImages']])
Z([3,'2'])
Z([3,'index'])
Z([3,'item'])
Z([[7],[3,'newMovies']])
Z(z[9])
Z(z[0])
Z(z[1])
Z(z[1])
Z([[7],[3,'item']])
Z([[4],[[5],[[5],[[4],[[5],[[5],[1,'^clickItem']],[[4],[[5],[[4],[[5],[1,'clickImage']]]]]]]],[[4],[[5],[[5],[1,'^clickMore']],[[4],[[5],[[4],[[5],[1,'more']]]]]]]]])
Z([[2,'+'],[[7],[3,'index']],[1,1]])
Z([[2,'+'],[1,'3-'],[[7],[3,'index']]])
})(__WXML_GLOBAL__.ops_cached.$gwx_9);return __WXML_GLOBAL__.ops_cached.$gwx_9
}
function gz$gwx_10(){
if( __WXML_GLOBAL__.ops_cached.$gwx_10)return __WXML_GLOBAL__.ops_cached.$gwx_10
__WXML_GLOBAL__.ops_cached.$gwx_10=[];
(function(z){var a=11;function Z(ops){z.push(ops)}
})(__WXML_GLOBAL__.ops_cached.$gwx_10);return __WXML_GLOBAL__.ops_cached.$gwx_10
}
__WXML_GLOBAL__.ops_set.$gwx=z;
__WXML_GLOBAL__.ops_init.$gwx=true;
var nv_require=function(){var nnm={};var nom={};return function(n){return function(){if(!nnm[n]) return undefined;try{if(!nom[n])nom[n]=nnm[n]();return nom[n];}catch(e){e.message=e.message.replace(/nv_/g,'');var tmp = e.stack.substring(0,e.stack.lastIndexOf(n));e.stack = tmp.substring(0,tmp.lastIndexOf('\n'));e.stack = e.stack.replace(/\snv_/g,' ');e.stack = $gstack(e.stack);e.stack += '\n    at ' + n.substring(2);console.error(e);}
}}}()
var x=['./components/mehaotian-search/mehaotian-search.wxml','./pages/components/zcx-grid/zcx-grid.wxml','./pages/components/zcx-swiper/zcx-swiper.wxml','./pages/detail/detail.wxml','./pages/play-movie/play-movie.wxml','./pages/play/play.wxml','./pages/tabBar/charts/charts.wxml','./pages/tabBar/hots/hots.wxml','./pages/tabBar/index/index.wxml','./pages/tabBar/my/my.wxml'];d_[x[0]]={}
var m0=function(e,s,r,gg){
var z=gz$gwx_1()
var oB=_mz(z,'view',['class',0,'style',1],[],e,s,gg)
var oD=_mz(z,'view',['class',2,'style',1],[],e,s,gg)
var fE=_n('view')
_rz(z,fE,'class',4,e,s,gg)
var hG=_n('text')
_rz(z,hG,'class',5,e,s,gg)
var oH=_oz(z,6,e,s,gg)
_(hG,oH)
_(fE,hG)
var cI=_mz(z,'input',['bindblur',7,'bindfocus',1,'bindinput',2,'class',3,'data-event-opts',4,'focus',5,'placeholder',6,'value',7],[],e,s,gg)
_(fE,cI)
var cF=_v()
_(fE,cF)
if(_oz(z,15,e,s,gg)){cF.wxVkey=1
var oJ=_mz(z,'text',['bindtap',16,'class',1,'data-event-opts',2],[],e,s,gg)
var lK=_oz(z,19,e,s,gg)
_(oJ,lK)
_(cF,oJ)
}
cF.wxXCkey=1
_(oD,fE)
var aL=_mz(z,'view',['bindtap',20,'class',1,'data-event-opts',2,'hidden',3],[],e,s,gg)
var tM=_oz(z,24,e,s,gg)
_(aL,tM)
_(oD,aL)
_(oB,oD)
var xC=_v()
_(oB,xC)
if(_oz(z,25,e,s,gg)){xC.wxVkey=1
var eN=_mz(z,'view',['bindtap',26,'class',1,'data-event-opts',2],[],e,s,gg)
var bO=_n('view')
_rz(z,bO,'class',29,e,s,gg)
var oP=_oz(z,30,e,s,gg)
_(bO,oP)
_(eN,bO)
_(xC,eN)
}
xC.wxXCkey=1
_(r,oB)
return r
}
e_[x[0]]={f:m0,j:[],i:[],ti:[],ic:[]}
d_[x[1]]={}
var m1=function(e,s,r,gg){
var z=gz$gwx_2()
var oR=_n('view')
var cT=_n('view')
_rz(z,cT,'class',0,e,s,gg)
var hU=_v()
_(cT,hU)
if(_oz(z,1,e,s,gg)){hU.wxVkey=1
var oV=_n('view')
_rz(z,oV,'class',2,e,s,gg)
var cW=_oz(z,3,e,s,gg)
_(oV,cW)
_(hU,oV)
}
else{hU.wxVkey=2
var oX=_n('view')
_rz(z,oX,'class',4,e,s,gg)
var lY=_oz(z,5,e,s,gg)
_(oX,lY)
_(hU,oX)
}
var aZ=_mz(z,'view',['bindtap',6,'class',1,'data-event-opts',2],[],e,s,gg)
var t1=_oz(z,9,e,s,gg)
_(aZ,t1)
_(cT,aZ)
hU.wxXCkey=1
_(oR,cT)
var e2=_mz(z,'view',['class',10,'noshade',1,'size',2],[],e,s,gg)
_(oR,e2)
var b3=_n('view')
_rz(z,b3,'class',13,e,s,gg)
var o4=_v()
_(b3,o4)
var x5=function(f7,o6,c8,gg){
var o0=_mz(z,'view',['bindtap',18,'class',1,'data-event-opts',2],[],f7,o6,gg)
var cAB=_mz(z,'image',['class',21,'mode',1,'src',2],[],f7,o6,gg)
_(o0,cAB)
var oBB=_n('view')
_rz(z,oBB,'class',24,f7,o6,gg)
var lCB=_oz(z,25,f7,o6,gg)
_(oBB,lCB)
_(o0,oBB)
_(c8,o0)
return c8
}
o4.wxXCkey=2
_2z(z,16,x5,e,s,gg,o4,'item','index','index')
_(oR,b3)
var aDB=_n('view')
_rz(z,aDB,'class',26,e,s,gg)
_(oR,aDB)
var fS=_v()
_(oR,fS)
if(_oz(z,27,e,s,gg)){fS.wxVkey=1
var tEB=_mz(z,'view',['bindtap',28,'class',1,'data-event-opts',2],[],e,s,gg)
var eFB=_oz(z,31,e,s,gg)
_(tEB,eFB)
_(fS,tEB)
}
fS.wxXCkey=1
_(r,oR)
return r
}
e_[x[1]]={f:m1,j:[],i:[],ti:[],ic:[]}
d_[x[2]]={}
var m2=function(e,s,r,gg){
var z=gz$gwx_3()
var oHB=_n('view')
var xIB=_mz(z,'swiper',['autoplay',0,'indicatorDots',1],[],e,s,gg)
var oJB=_v()
_(xIB,oJB)
var fKB=function(hMB,cLB,oNB,gg){
var oPB=_mz(z,'swiper-item',['bindtap',6,'data-event-opts',1],[],hMB,cLB,gg)
var lQB=_n('view')
var aRB=_mz(z,'image',['mode',8,'src',1,'style',2],[],hMB,cLB,gg)
_(lQB,aRB)
_(oPB,lQB)
_(oNB,oPB)
return oNB
}
oJB.wxXCkey=2
_2z(z,4,fKB,e,s,gg,oJB,'item','index','index')
_(oHB,xIB)
_(r,oHB)
return r
}
e_[x[2]]={f:m2,j:[],i:[],ti:[],ic:[]}
d_[x[3]]={}
var m3=function(e,s,r,gg){
var z=gz$gwx_4()
var eTB=_n('view')
var bUB=_n('view')
_rz(z,bUB,'class',0,e,s,gg)
var oVB=_mz(z,'image',['class',1,'mode',1,'src',2],[],e,s,gg)
_(bUB,oVB)
var xWB=_n('view')
_rz(z,xWB,'class',4,e,s,gg)
var oXB=_n('view')
_rz(z,oXB,'class',5,e,s,gg)
var fYB=_oz(z,6,e,s,gg)
_(oXB,fYB)
_(xWB,oXB)
var cZB=_n('view')
_rz(z,cZB,'class',7,e,s,gg)
var h1B=_n('view')
_rz(z,h1B,'class',8,e,s,gg)
var o2B=_oz(z,9,e,s,gg)
_(h1B,o2B)
_(cZB,h1B)
var c3B=_oz(z,10,e,s,gg)
_(cZB,c3B)
_(xWB,cZB)
var o4B=_n('view')
_rz(z,o4B,'class',11,e,s,gg)
var l5B=_n('view')
_rz(z,l5B,'class',12,e,s,gg)
var a6B=_oz(z,13,e,s,gg)
_(l5B,a6B)
_(o4B,l5B)
var t7B=_oz(z,14,e,s,gg)
_(o4B,t7B)
_(xWB,o4B)
var e8B=_n('view')
_rz(z,e8B,'class',15,e,s,gg)
var b9B=_n('view')
_rz(z,b9B,'class',16,e,s,gg)
var o0B=_oz(z,17,e,s,gg)
_(b9B,o0B)
_(e8B,b9B)
var xAC=_oz(z,18,e,s,gg)
_(e8B,xAC)
_(xWB,e8B)
var oBC=_n('view')
_rz(z,oBC,'class',19,e,s,gg)
var fCC=_n('view')
_rz(z,fCC,'class',20,e,s,gg)
var cDC=_oz(z,21,e,s,gg)
_(fCC,cDC)
_(oBC,fCC)
var hEC=_oz(z,22,e,s,gg)
_(oBC,hEC)
_(xWB,oBC)
var oFC=_n('view')
_rz(z,oFC,'class',23,e,s,gg)
var cGC=_n('view')
_rz(z,cGC,'class',24,e,s,gg)
var oHC=_oz(z,25,e,s,gg)
_(cGC,oHC)
_(oFC,cGC)
var lIC=_oz(z,26,e,s,gg)
_(oFC,lIC)
_(xWB,oFC)
_(bUB,xWB)
_(eTB,bUB)
var aJC=_n('view')
_rz(z,aJC,'class',27,e,s,gg)
var eLC=_n('view')
_rz(z,eLC,'class',28,e,s,gg)
var bMC=_oz(z,29,e,s,gg)
_(eLC,bMC)
_(aJC,eLC)
var oNC=_n('view')
_rz(z,oNC,'class',30,e,s,gg)
_(aJC,oNC)
var xOC=_mz(z,'view',['class',31,'style',1],[],e,s,gg)
var oPC=_n('view')
_rz(z,oPC,'class',33,e,s,gg)
var fQC=_oz(z,34,e,s,gg)
_(oPC,fQC)
_(xOC,oPC)
var cRC=_oz(z,35,e,s,gg)
_(xOC,cRC)
_(aJC,xOC)
var tKC=_v()
_(aJC,tKC)
if(_oz(z,36,e,s,gg)){tKC.wxVkey=1
var hSC=_mz(z,'view',['bindtap',37,'class',1,'data-event-opts',2,'hidden',3],[],e,s,gg)
var oTC=_oz(z,41,e,s,gg)
_(hSC,oTC)
_(tKC,hSC)
}
else{tKC.wxVkey=2
var cUC=_mz(z,'view',['bindtap',42,'class',1,'data-event-opts',2,'hidden',3],[],e,s,gg)
var oVC=_oz(z,46,e,s,gg)
_(cUC,oVC)
_(tKC,cUC)
}
tKC.wxXCkey=1
_(eTB,aJC)
var lWC=_n('view')
_rz(z,lWC,'class',47,e,s,gg)
var aXC=_mz(z,'button',['bindtap',48,'data-event-opts',1],[],e,s,gg)
var tYC=_oz(z,50,e,s,gg)
_(aXC,tYC)
_(lWC,aXC)
var eZC=_mz(z,'button',['bindtap',51,'data-event-opts',1],[],e,s,gg)
var b1C=_oz(z,53,e,s,gg)
_(eZC,b1C)
_(lWC,eZC)
_(eTB,lWC)
_(r,eTB)
return r
}
e_[x[3]]={f:m3,j:[],i:[],ti:[],ic:[]}
d_[x[4]]={}
var m4=function(e,s,r,gg){
var z=gz$gwx_5()
var x3C=_n('view')
var o4C=_mz(z,'web-view',['class',0,'hidden',1,'src',1,'webviewStyles',2],[],e,s,gg)
_(x3C,o4C)
_(r,x3C)
return r
}
e_[x[4]]={f:m4,j:[],i:[],ti:[],ic:[]}
d_[x[5]]={}
var m5=function(e,s,r,gg){
var z=gz$gwx_6()
var c6C=_n('view')
var h7C=_mz(z,'video',['danmuBtn',-1,'enableDanmu',-1,'pageGesture',-1,'binderror',0,'class',1,'data-event-opts',1,'data-ref',2,'id',3,'objectFit',4,'src',5],[],e,s,gg)
_(c6C,h7C)
var o8C=_v()
_(c6C,o8C)
var c9C=function(lAD,o0C,aBD,gg){
var eDD=_n('view')
_rz(z,eDD,'class',11,lAD,o0C,gg)
var bED=_n('view')
_rz(z,bED,'class',12,lAD,o0C,gg)
var oFD=_oz(z,13,lAD,o0C,gg)
_(bED,oFD)
_(eDD,bED)
var xGD=_mz(z,'view',['class',14,'noshade',1,'size',2],[],lAD,o0C,gg)
_(eDD,xGD)
var oHD=_n('view')
_rz(z,oHD,'class',17,lAD,o0C,gg)
var fID=_v()
_(oHD,fID)
var cJD=function(oLD,hKD,cMD,gg){
var lOD=_mz(z,'view',['bindtap',22,'class',1,'data-event-opts',2],[],oLD,hKD,gg)
var aPD=_oz(z,25,oLD,hKD,gg)
_(lOD,aPD)
_(cMD,lOD)
return cMD
}
fID.wxXCkey=2
_2z(z,20,cJD,lAD,o0C,gg,fID,'item','index','index')
_(eDD,oHD)
_(aBD,eDD)
return aBD
}
o8C.wxXCkey=2
_2z(z,9,c9C,e,s,gg,o8C,'value','name','name')
_(r,c6C)
return r
}
e_[x[5]]={f:m5,j:[],i:[],ti:[],ic:[]}
d_[x[6]]={}
var m6=function(e,s,r,gg){
var z=gz$gwx_7()
var eRD=_n('view')
_(r,eRD)
return r
}
e_[x[6]]={f:m6,j:[],i:[],ti:[],ic:[]}
d_[x[7]]={}
var m7=function(e,s,r,gg){
var z=gz$gwx_8()
var oTD=_n('view')
_(r,oTD)
return r
}
e_[x[7]]={f:m7,j:[],i:[],ti:[],ic:[]}
d_[x[8]]={}
var m8=function(e,s,r,gg){
var z=gz$gwx_9()
var oVD=_n('view')
var fWD=_mz(z,'m-search',['bind:__l',0,'bind:search',1,'data-event-opts',1,'vueId',2],[],e,s,gg)
_(oVD,fWD)
var cXD=_mz(z,'zcx-swiper',['bind:__l',4,'bind:clickImage',1,'data-event-opts',2,'images',3,'vueId',4],[],e,s,gg)
_(oVD,cXD)
var hYD=_v()
_(oVD,hYD)
var oZD=function(o2D,c1D,l3D,gg){
var t5D=_mz(z,'zcx-grid',['bind:__l',13,'bind:clickItem',1,'bind:clickMore',2,'contents',3,'data-event-opts',4,'type',5,'vueId',6],[],o2D,c1D,gg)
_(l3D,t5D)
return l3D
}
hYD.wxXCkey=4
_2z(z,11,oZD,e,s,gg,hYD,'item','index','index')
_(r,oVD)
return r
}
e_[x[8]]={f:m8,j:[],i:[],ti:[],ic:[]}
d_[x[9]]={}
var m9=function(e,s,r,gg){
var z=gz$gwx_10()
var b7D=_n('view')
_(r,b7D)
return r
}
e_[x[9]]={f:m9,j:[],i:[],ti:[],ic:[]}
if(path&&e_[path]){
window.__wxml_comp_version__=0.02
return function(env,dd,global){$gwxc=0;var root={"tag":"wx-page"};root.children=[]
var main=e_[path].f
if (typeof global==="undefined")global={};global.f=$gdc(f_[path],"",1);
if(typeof(window.__webview_engine_version__)!='undefined'&&window.__webview_engine_version__+1e-6>=0.02+1e-6&&window.__mergeData__)
{
env=window.__mergeData__(env,dd);
}
try{
main(env,{},root,global);
_tsd(root)
if(typeof(window.__webview_engine_version__)=='undefined'|| window.__webview_engine_version__+1e-6<0.01+1e-6){return _ev(root);}
}catch(err){
console.log(err)
}
return root;
}
}
}


var BASE_DEVICE_WIDTH = 750;
var isIOS=navigator.userAgent.match("iPhone");
var deviceWidth = window.screen.width || 375;
var deviceDPR = window.devicePixelRatio || 2;
var checkDeviceWidth = window.__checkDeviceWidth__ || function() {
var newDeviceWidth = window.screen.width || 375
var newDeviceDPR = window.devicePixelRatio || 2
var newDeviceHeight = window.screen.height || 375
if (window.screen.orientation && /^landscape/.test(window.screen.orientation.type || '')) newDeviceWidth = newDeviceHeight
if (newDeviceWidth !== deviceWidth || newDeviceDPR !== deviceDPR) {
deviceWidth = newDeviceWidth
deviceDPR = newDeviceDPR
}
}
checkDeviceWidth()
var eps = 1e-4;
var transformRPX = window.__transformRpx__ || function(number, newDeviceWidth) {
if ( number === 0 ) return 0;
number = number / BASE_DEVICE_WIDTH * ( newDeviceWidth || deviceWidth );
number = Math.floor(number + eps);
if (number === 0) {
if (deviceDPR === 1 || !isIOS) {
return 1;
} else {
return 0.5;
}
}
return number;
}
var setCssToHead = function(file, _xcInvalid, info) {
var Ca = {};
var css_id;
var info = info || {};
var _C= [[[2,1],],[],];
function makeup(file, opt) {
var _n = typeof(file) === "number";
if ( _n && Ca.hasOwnProperty(file)) return "";
if ( _n ) Ca[file] = 1;
var ex = _n ? _C[file] : file;
var res="";
for (var i = ex.length - 1; i >= 0; i--) {
var content = ex[i];
if (typeof(content) === "object")
{
var op = content[0];
if ( op == 0 )
res = transformRPX(content[1], opt.deviceWidth) + "px" + res;
else if ( op == 1)
res = opt.suffix + res;
else if ( op == 2 ) 
res = makeup(content[1], opt) + res;
}
else
res = content + res
}
return res;
}
var rewritor = function(suffix, opt, style){
opt = opt || {};
suffix = suffix || "";
opt.suffix = suffix;
if ( opt.allowIllegalSelector != undefined && _xcInvalid != undefined )
{
if ( opt.allowIllegalSelector )
console.warn( "For developer:" + _xcInvalid );
else
{
console.error( _xcInvalid + "This wxss file is ignored." );
return;
}
}
Ca={};
css = makeup(file, opt);
if ( !style ) 
{
var head = document.head || document.getElementsByTagName('head')[0];
window.__rpxRecalculatingFuncs__ = window.__rpxRecalculatingFuncs__ || [];
style = document.createElement('style');
style.type = 'text/css';
style.setAttribute( "wxss:path", info.path );
head.appendChild(style);
window.__rpxRecalculatingFuncs__.push(function(size){
opt.deviceWidth = size.width;
rewritor(suffix, opt, style);
});
}
if (style.styleSheet) {
style.styleSheet.cssText = css;
} else {
if ( style.childNodes.length == 0 )
style.appendChild(document.createTextNode(css));
else 
style.childNodes[0].nodeValue = css;
}
}
return rewritor;
}
setCssToHead([])();setCssToHead([[2,0]],undefined,{path:"./app.wxss"})();

__wxAppCode__['app.wxss']=setCssToHead([[2,0]],undefined,{path:"./app.wxss"});    
__wxAppCode__['app.wxml']=$gwx('./app.wxml');

__wxAppCode__['components/mehaotian-search/mehaotian-search.wxss']=setCssToHead(["@charset \x22UTF-8\x22;\n.",[1],"search.",[1],"data-v-6e03cdbc { display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; width: 100%; border-bottom: 1px #f5f5f5 solid; -webkit-box-sizing: border-box; box-sizing: border-box; padding: ",[0,15],"; font-size: ",[0,28],"; background: #fff; }\n.",[1],"search .",[1],"content.",[1],"data-v-6e03cdbc { display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; width: 100%; height: ",[0,60],"; border: 1px #ccc solid; background: #fff; overflow: hidden; -webkit-transition: all 0.2s linear; -o-transition: all 0.2s linear; transition: all 0.2s linear; border-radius: 30px; }\n.",[1],"search .",[1],"content .",[1],"content-box.",[1],"data-v-6e03cdbc { width: 100%; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; }\n.",[1],"search .",[1],"content .",[1],"content-box.",[1],"center.",[1],"data-v-6e03cdbc { -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; }\n.",[1],"search .",[1],"content .",[1],"content-box .",[1],"icon.",[1],"data-v-6e03cdbc { padding: 0 ",[0,15],"; }\n.",[1],"search .",[1],"content .",[1],"content-box .",[1],"icon.",[1],"icon-del.",[1],"data-v-6e03cdbc { font-size: ",[0,38],"; }\n.",[1],"search .",[1],"content .",[1],"content-box .",[1],"input.",[1],"data-v-6e03cdbc { width: 100%; max-width: 100%; line-height: ",[0,60],"; height: ",[0,60],"; -webkit-transition: all 0.2s linear; -o-transition: all 0.2s linear; transition: all 0.2s linear; }\n.",[1],"search .",[1],"content .",[1],"content-box .",[1],"input.",[1],"center.",[1],"data-v-6e03cdbc { width: ",[0,200],"; }\n.",[1],"search .",[1],"content .",[1],"content-box .",[1],"input.",[1],"sub.",[1],"data-v-6e03cdbc { width: auto; color: grey; }\n.",[1],"search .",[1],"content .",[1],"searchBtn.",[1],"data-v-6e03cdbc { height: 100%; -webkit-flex-shrink: 0; -ms-flex-negative: 0; flex-shrink: 0; padding: 0 ",[0,30],"; background: #4cd964; line-height: ",[0,60],"; color: #fff; border-left: 1px #ccc solid; -webkit-transition: all 0.3s; -o-transition: all 0.3s; transition: all 0.3s; }\n.",[1],"search .",[1],"button.",[1],"data-v-6e03cdbc { display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; position: relative; -webkit-flex-shrink: 0; -ms-flex-negative: 0; flex-shrink: 0; width: 0; -webkit-transition: all 0.2s linear; -o-transition: all 0.2s linear; transition: all 0.2s linear; white-space: nowrap; overflow: hidden; }\n.",[1],"search .",[1],"button.",[1],"active.",[1],"data-v-6e03cdbc { padding-left: ",[0,15],"; width: ",[0,100],"; }\n@font-face { font-family: \x27iconfont\x27; src: url(\x22https://at.alicdn.com/t/font_989023_efq0mtli526.ttf\x22) format(\x22truetype\x22); }\n.",[1],"icon.",[1],"data-v-6e03cdbc { font-family: iconfont; font-size: ",[0,32],"; font-style: normal; color: #999; }\n",],undefined,{path:"./components/mehaotian-search/mehaotian-search.wxss"});    
__wxAppCode__['components/mehaotian-search/mehaotian-search.wxml']=$gwx('./components/mehaotian-search/mehaotian-search.wxml');

__wxAppCode__['pages/components/zcx-grid/zcx-grid.wxss']=setCssToHead(["@charset \x22UTF-8\x22;\n.",[1],"header { margin: ",[0,20]," 0; }\n.",[1],"header .",[1],"title { display: inline-block; font-size: ",[0,40],"; line-height: ",[0,40],"; color: #333333; margin-left: ",[0,20],"; padding-top: ",[0,25],"; padding-bottom: ",[0,15],"; float: left; }\n.",[1],"header .",[1],"more { display: inline-block; font-size: ",[0,20],"; line-height: ",[0,20],"; color: #999999; margin-right: ",[0,20],"; padding-top: ",[0,10],"; float: right; }\n.",[1],"_hr { margin: ",[0,10]," 0; color: #007AFF; clear: both; }\n.",[1],"flex-row { display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-orient: horizontal; -webkit-box-direction: normal; -webkit-flex-flow: row wrap; -ms-flex-flow: row wrap; flex-flow: row wrap; -webkit-box-pack: start; -webkit-justify-content: flex-start; -ms-flex-pack: start; justify-content: flex-start; -webkit-align-content: stretch; -ms-flex-line-pack: stretch; align-content: stretch; -webkit-box-align: stretch; -webkit-align-items: stretch; -ms-flex-align: stretch; align-items: stretch; width: 100vw; }\n.",[1],"flex-row .",[1],"flex-row-item { width: 32vw; height: ",[0,380],"; margin: calc(4vw / 6) calc(4vw / 6); display: inline-block; -webkit-box-flex: 0; -webkit-flex: 0 0 32vw; -ms-flex: 0 0 32vw; flex: 0 0 32vw; background: #F1F1F1; }\n.",[1],"flex-row .",[1],"flex-row-item .",[1],"flex-row-item_image { width: 32vw; height: ",[0,300],"; }\n.",[1],"flex-row .",[1],"flex-row-item .",[1],"flex-row-item_title { height: ",[0,100],"; width: 32vw; font-size: ",[0,30],"; -o-text-overflow: ellipsis; text-overflow: ellipsis; white-space: nowrap; overflow: hidden; line-height: ",[0,50],"; text-align: center; }\n.",[1],"more { color: #F0AD4E; height: ",[0,35],"; font-size: ",[0,25],"; line-height: ",[0,25],"; margin: ",[0,25],"; text-align: center; }\n",],undefined,{path:"./pages/components/zcx-grid/zcx-grid.wxss"});    
__wxAppCode__['pages/components/zcx-grid/zcx-grid.wxml']=$gwx('./pages/components/zcx-grid/zcx-grid.wxml');

__wxAppCode__['pages/components/zcx-swiper/zcx-swiper.wxss']=undefined;    
__wxAppCode__['pages/components/zcx-swiper/zcx-swiper.wxml']=$gwx('./pages/components/zcx-swiper/zcx-swiper.wxml');

__wxAppCode__['pages/detail/detail.wxss']=setCssToHead(["@charset \x22UTF-8\x22;\n.",[1],"button-hover { background: #999999; }\n.",[1],"header { height: ",[0,420],"; background: #C8C7CC; }\n.",[1],"header .",[1],"header-image { width: ",[0,300],"; height: ",[0,400],"; }\n.",[1],"header .",[1],"header-texts { width: calc(100vw - ",[0,340],"); height: ",[0,400],"; margin-left: ",[0,40],"; display: inline-block; }\n.",[1],"header .",[1],"header-texts .",[1],"one { font-size: ",[0,40],"; line-height: ",[0,160],"; }\n.",[1],"header .",[1],"header-texts .",[1],"two { font-size: ",[0,35],"; line-height: ",[0,50],"; -o-text-overflow: ellipsis; text-overflow: ellipsis; white-space: nowrap; overflow: hidden; }\n.",[1],"intro-box { margin-top: ",[0,20],"; background: #C8C7CC; }\n.",[1],"intro-box .",[1],"top { font-size: ",[0,45],"; padding: ",[0,25],"; }\n.",[1],"intro-box .",[1],"intro { height: ",[0,320],"; font-size: ",[0,35],"; overflow: hidden; }\n.",[1],"intro-box .",[1],"more { float: right; font-size: ",[0,20],"; width: ",[0,180],"; background: #F0AD4E; color: #333333; text-align: center; margin: ",[0,10]," ",[0,10],"; }\n.",[1],"btn-group { margin-top: ",[0,80],"; }\n.",[1],"gray-text { color: #808080; font-size: ",[0,30],"; display: inline; }\n",],undefined,{path:"./pages/detail/detail.wxss"});    
__wxAppCode__['pages/detail/detail.wxml']=$gwx('./pages/detail/detail.wxml');

__wxAppCode__['pages/play-movie/play-movie.wxss']=setCssToHead(["@charset \x22UTF-8\x22;\n.",[1],"webView { width: 100vw; height: 100vh; }\n",],undefined,{path:"./pages/play-movie/play-movie.wxss"});    
__wxAppCode__['pages/play-movie/play-movie.wxml']=$gwx('./pages/play-movie/play-movie.wxml');

__wxAppCode__['pages/play/play.wxss']=setCssToHead(["@charset \x22UTF-8\x22;\nwx-video { width: 100vw; }\nwx-web-view { width: 100vw; height: 100vh; }\n.",[1],"source { width: 100vw; background: #F1F1F1; margin-top: ",[0,20],"; }\n.",[1],"source .",[1],"name { font-size: ",[0,60],"; margin: ",[0,40]," ",[0,40],"; }\n.",[1],"source .",[1],"flex-box { display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; margin-top: ",[0,10],"; -webkit-box-orient: horizontal; -webkit-box-direction: normal; -webkit-flex-flow: row wrap; -ms-flex-flow: row wrap; flex-flow: row wrap; -webkit-box-pack: start; -webkit-justify-content: flex-start; -ms-flex-pack: start; justify-content: flex-start; -webkit-align-content: stretch; -ms-flex-line-pack: stretch; align-content: stretch; -webkit-box-align: stretch; -webkit-align-items: stretch; -ms-flex-align: stretch; align-items: stretch; width: 100vw; }\n.",[1],"source .",[1],"flex-box .",[1],"flex-item { width: 15vw; height: ",[0,100],"; text-align: center; margin: calc(10vw / 12) calc(10vw / 12); -webkit-box-flex: 0; -webkit-flex: 0 0 15vw; -ms-flex: 0 0 15vw; flex: 0 0 15vw; background: #999999; font-size: ",[0,40],"; line-height: ",[0,80],"; }\n.",[1],"source .",[1],"flex-box .",[1],"flex-item:hover { background: white; }\n",],undefined,{path:"./pages/play/play.wxss"});    
__wxAppCode__['pages/play/play.wxml']=$gwx('./pages/play/play.wxml');

__wxAppCode__['pages/tabBar/charts/charts.wxss']=undefined;    
__wxAppCode__['pages/tabBar/charts/charts.wxml']=$gwx('./pages/tabBar/charts/charts.wxml');

__wxAppCode__['pages/tabBar/hots/hots.wxss']=undefined;    
__wxAppCode__['pages/tabBar/hots/hots.wxml']=$gwx('./pages/tabBar/hots/hots.wxml');

__wxAppCode__['pages/tabBar/index/index.wxss']=undefined;    
__wxAppCode__['pages/tabBar/index/index.wxml']=$gwx('./pages/tabBar/index/index.wxml');

__wxAppCode__['pages/tabBar/my/my.wxss']=undefined;    
__wxAppCode__['pages/tabBar/my/my.wxml']=$gwx('./pages/tabBar/my/my.wxml');

;var __pageFrameEndTime__ = Date.now();
(function() {
        window.UniLaunchWebviewReady = function(isWebviewReady){
          // !isWebviewReady && console.log('launchWebview fallback ready')
          plus.webview.postMessageToUniNView({type: 'UniWebviewReady-' + plus.webview.currentWebview().id}, '__uniapp__service');
        }
        UniLaunchWebviewReady(true);
})();
